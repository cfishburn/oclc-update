# frozen_string_literal: true

Rails.application.routes.draw do
  get 'pass_one/index'
  get 'pass_one/compare'
  post 'pass_one/compare'
  get 'pass_one/upload'
  post 'pass_one/upload'
  post 'download_csv', to: 'pass_one#download_csv'
  # post 'pass_one/download'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'pass_one#index'
end
