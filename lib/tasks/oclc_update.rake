# frozen_string_literal: true

require 'rake'
require 'csv'
desc 'This is a test'

task :oclc_first_pass, [:oclc_name, :pub_name, :output_name] do |_, args|
  @pub_file_hash = {}
  @oclc_file_hash = {}
  File.open(Rails.root.join('public', 'uploads',
                            args[:pub_name]), 'rb') do |file|
    file.each_line do |line|
      # puts line
      line_data = line.split(',')
      @pub_file_hash[line_data[0]] = line_data[1].strip
    end
  end
  File.open(Rails.root.join('public', 'uploads',
                            args[:oclc_name]), 'rb') do |file|
    file.each_line do |line|
      line_data = line.split(',')
      @oclc_file_hash[line_data[0]] = line_data[1].strip
    end
  end
  # go through published file (ours)
  # if the key matches and the oclc number matches, move to next line
  # if the key has no match in oclc file, write that key/value pair to file
  # if they key matches but oclc number doesn't, write to new file
  CSV.open(Rails.root.join('public', 'downloads',
                           args[:output_name]), 'wb') do |csv|
    csv << ['MMS_ID', 'OCLC number']
  end
  @pub_file_hash.each do |key, value|
    next unless @oclc_file_hash.key?(key)

    @compare_value = @oclc_file_hash[key]
    while @compare_value.length < 8 do
      @compare_value.prepend('0')
    end
    @compare_value = @compare_value.strip + ';'
    next if value.include? @compare_value

    CSV.open(Rails.root.join('public', 'downloads',
                             args[:output_name]), 'a+') do |csv|
      csv << [key.to_s, @compare_value]
    end
  end
end

task :oclc_second_pass, [:institution_code, :file_name] => :environment do |_, args|
  institution = args[:institution_code]
  file = Rails.root.join('public', 'downloads', args[:file_name])
  data = []

  CSV.foreach(file, headers: true) do |row|
    data << row.to_hash
  end
  CSV.open(Rails.root.join('public', 'API',
                           'api_errors.csv'), 'wb') do |csv|
    csv << ['MMS_ID', 'OCLC number', 'NZ MMS_ID']
  end
  data.each do |bib|
    if bib['NZ MMS_ID'].blank?
      @api_institution = institution
      @api_mms_id = bib['MMS_ID']
    else
      @api_institution = 'nz'
      @api_mms_id = bib['NZ MMS_ID']
    end
    puts @api_institution + ', ' + @api_mms_id
    @bib_api = AlmaBibsApi.new @api_institution
    bib_xml = @bib_api.get_by_mmsid @api_mms_id
    xml_doc = Nokogiri::XML(bib_xml).remove_namespaces!
    old_xml_doc = xml_doc.css('datafield[tag=035]')
    first_oclc = xml_doc.css('datafield[tag=035]').first
    if old_xml_doc.empty?
      CSV.open(Rails.root.join('public', 'API',
                               'api_errors.csv'), 'a+') do |csv|
        row = [bib['MMS ID'], bib['OCLC number'], bib['NZ MMS ID']]
        csv << row
      end
      next
    end

    @new_xml_doc = @bib_api.build_new_oclc_block(xml_doc, bib['OCLC number'])
    first_oclc.before(@new_xml_doc)
    old_xml_doc.remove
    @bib_api.update(@api_mms_id, xml_doc.to_s)
    # puts xml_doc
  end
  puts data
  puts 'Second pass successful'
end

task :oclc_testing_pass, [:oclc_name, :pub_name] do |_, args|
  # this pass will determine validity of the input files before I move forward
  @pub_file_hash = {}
  @oclc_file_hash = {}
  pub_count = 0
  oclc_count = 0
  File.open(Rails.root.join('public', 'uploads',
                            args[:pub_name]), 'rb') do |file|
    file.each_line do |line|
      #puts line
      pub_count += 1
      line_data = line.split(',')
      if line_data[1].nil?
        # puts 'Published File errors'
        puts line_data[0].to_s + ' ' + pub_count.to_s
        next
      end
      @pub_file_hash[line_data[0]] = line_data[1].strip
    end
  end
  File.open(Rails.root.join('public', 'uploads',
                            args[:oclc_name]), 'rb') do |file|
    file.each_line do |line|
      # puts line
      oclc_count += 1
      line_data = line.split(',')
      if line_data[1].nil?
        # puts 'OCLC File errors'
        puts line_data[0].to_s + ' ' + oclc_count.to_s
        next
      end
      @oclc_file_hash[line_data[0]] = line_data[1].strip
    end
  end
  puts 'Test Pass Successful'
end

task :oclc_mga_update, [:institution_code, :pub_name] => :environment do |_, args|
  institution = args[:institution_code]
  file = Rails.root.join('public', 'downloads', args[:pub_name])
  data = []
  CSV.foreach(file, headers: true) do |row|
    data << row.to_hash
  end
  #puts data
  data.each do |bib|
    @api_mms_id = bib['MMS_ID']
    puts 'Updating mga ' + @api_mms_id
    # puts @api_institution + ', ' + @api_mms_id
    @bib_api = AlmaBibsApi.new institution
    bib_xml = @bib_api.get_by_mmsid @api_mms_id
    xml_doc = Nokogiri::XML(bib_xml).remove_namespaces!
    old_xml_doc = xml_doc.css('datafield[tag=035]')
    first_oclc = xml_doc.css('datafield[tag=035]').first

    @new_xml_doc = @bib_api.build_new_oclc_block_mga(xml_doc, bib['OCLC number'])
    first_oclc.before(@new_xml_doc)
    first_oclc.remove
    @bib_api.update(@api_mms_id, xml_doc.to_s)
    # puts xml_doc
    # puts xml_doc if bib['MMS_ID'] = '997007904602955'
  end
  puts 'mga pass successful'
end

task :highlands_first_pass, [:oclc_name, :pub_name, :output_name] do |_, args|
  @pub_file_array = []
  @oclc_file_hash = {}
  @removed_hash = {}
  @update_hash = {}
  File.open(Rails.root.join('public', 'uploads',
                            args[:pub_name]), 'rb') do |file|
    file.each_line do |line|
      # puts line
      @pub_file_array << line.strip.to_s
    end
  end
  # puts @pub_file_array.include?('99323253702946')
  File.open(Rails.root.join('public', 'uploads',
                            args[:oclc_name]), 'rb') do |file|
    file.each_line do |line|
      line_data = line.split(',')
      @oclc_file_hash[line_data[0]] = line_data[1].strip
    end
  end

  CSV.open(Rails.root.join('public', 'downloads',
                           args[:output_name]), 'wb') do |csv|
    csv << ['MMS_ID', 'OCLC number']
  end

  @oclc_file_hash.each do |mmsid, oclc|
    #puts mmsid
    next if @pub_file_array.include? mmsid
    @compare_value = oclc
    while @compare_value.length < 8 do
      @compare_value.prepend('0')
    end
    CSV.open(Rails.root.join('public', 'downloads',
                             args[:output_name]), 'a+') do |csv|
      csv << [mmsid.to_s, @compare_value]
    end
  end
end