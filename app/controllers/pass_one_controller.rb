# frozen_string_literal: true

require 'csv'
class PassOneController < ApplicationController
  def index
    unless Dir.empty?(Rails.root.join('public', 'uploads'))
      Dir.glob(Rails.root.join('public', 'uploads', '*'))
         .each { |file| File.delete(file) }
    end
    unless Dir.empty?(Rails.root.join('public', 'downloads'))
      Dir.glob(Rails.root.join('public', 'downloads', '*.csv'))
         .each { |file| File.delete(file) }
    end
    puts 'went to index'
  end

  def upload
    pub_file_io = params[:pub_file]
    oclc_file_io = params[:oclc_file]
    File.open(Rails.root.join('public', 'uploads',
                              pub_file_io.original_filename), 'wb') do |file|
      file.write(pub_file_io.read)
    end

    File.open(Rails.root.join('public', 'uploads',
                              oclc_file_io.original_filename), 'wb') do |file|
      file.write(oclc_file_io.read)
    end
    redirect_to :action => 'compare',
                :pub_name => pub_file_io.original_filename,
                :oclc_name => oclc_file_io.original_filename
  end

  def compare
    @pub_file_hash = {}
    @oclc_file_hash = {}
    File.open(Rails.root.join('public', 'uploads',
                              params[:pub_name]), 'rb') do |file|
      file.each_line do |line|
        line_data = line.split(',')
        @pub_file_hash[line_data[0]] = line_data[1].strip
      end
    end
    File.open(Rails.root.join('public', 'uploads',
                              params[:oclc_name]), 'rb') do |file|
      file.each_line do |line|
        line_data = line.split(',')
        @oclc_file_hash[line_data[0]] = line_data[1].strip
      end
    end
    # go through published file (ours)
    # if the key matches and the oclc number matches, move to next line
    # if the key has no match in oclc file, write that key/value pair to file
    # if they key matches but oclc number doesn't, write to new file
    CSV.open(Rails.root.join('public', 'downloads',
                             'compare.csv'), 'wb') do |csv|
      csv << ['MMS_ID', 'OCLC number']
    end
    @pub_file_hash.each do |key, value|
      next unless @oclc_file_hash.key?(key)

      puts 'got past check for match'
      @compare_value = @oclc_file_hash[key]
      while @compare_value.length < 8 do
        @compare_value.prepend('0')
      end
      @compare_value = @compare_value.strip + ';'
      next if value.include? @compare_value

      CSV.open(Rails.root.join('public', 'downloads',
                               'compare.csv'), 'a+') do |csv|
        csv << [key.to_s, @oclc_file_hash[key].strip.tr('"', '')]
      end
    end
  end

  def download_csv
    File.open(Rails.root.join('public', 'downloads', 'compare.csv'), 'r') do |f|
      send_data(f.read, type: 'text/csv')
    end
    Dir.glob(Rails.root.join('public', 'downloads', '*.csv'))
       .each { |file| File.delete(file) }
    Dir.glob(Rails.root.join('public', 'uploads', '*'))
       .each { |file| File.delete(file) }
  end
end
