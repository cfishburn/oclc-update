# frozen_string_literal: true

class AlmaBibsApi < AlmaApi
  def initialize(institution, notifier = DEFAULT_NOTIFIER)
    super(institution, notifier)
    @url = 'https://api-na.hosted.exlibrisgroup.com/almaws/v1/bibs/'
    @params[:user_id_type] = 'all_unique'
  end

  def get_by_mmsid(mms_id)
    raise ArgumentError, "mms_id can't be blank" if mms_id.blank?

    @params[:view] = 'full'
    @params[:expand] = 'none'
    request_config = { method: 'GET',
                       url: @url + mms_id,
                       headers: @headers }
    api_request request_config
  end

  def build_new_oclc_block(old_xml_doc, new_oclc_number)
    old_datfield = old_xml_doc.css('datafield[tag=035]') # all datafield's that are 035
    new_datafield = '<datafield ind1="" ind2="" tag="035">
                      <subfield code="a">(OCoLC)' + new_oclc_number +
                    '</subfield>'
    old_datfield.children.each do |sub|
      new_datafield << if sub.text.include? 'EXLNZ'
                         '<subfield code="a">' + sub.text + '</subfield>'
                       else
                         '<subfield code="z">' + sub.text + '</subfield>'
                       end
    end
    new_datafield << '</datafield>'
    new_datafield
  end

  def update(mms_id, xml_updated_bib)
    raise ArgumentError, "mms_id can't be blank" if mms_id.blank?

    # Validate xml; Nokogiri will raise an error if not valid.
    Nokogiri::XML(xml_updated_bib, &:strict)

    @params[:send_pin_number_letter] = 'false'
    @params[:expand] = 'none'
    @headers[:content_type] = 'application/xml'
    request_config = { method: 'PUT',
                       url: @url + mms_id,
                       headers: @headers,
                       payload: xml_updated_bib }
    api_request request_config
  end

  def build_new_oclc_block_mga(old_xml_doc, new_oclc_number)
    old_datfield = old_xml_doc.css('datafield[tag=035]') # all datafield's that are 035
    new_datafield = '<datafield ind1="" ind2="" tag="035">
                      <subfield code="a">(OCoLC)' + new_oclc_number +
                    '</subfield>'

    new_datafield << '</datafield>'
    new_datafield
  end

end