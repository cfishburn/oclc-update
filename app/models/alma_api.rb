# frozen_string_literal: true

class AlmaApi
  include RestClient
  BASE_URL = 'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/'
  DEFAULT_NOTIFIER = Slack::Notifier.new(Rails.application.secrets.slack_worker_webhook)

  attr_accessor :limit, :params

  # @param [Institution] institution
  # @param [Slack::Notifier] notifier
  def initialize(institution, notifier = DEFAULT_NOTIFIER)
    @institution = institution
    @api_key = Rails.application.secrets.send("#{institution}_api_key")
    @notifier = notifier
    @limit = '1000'
    @max_retries = 3
    @url = 'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/'
    @params = {
      limit: @limit,
      apikey: @api_key
    }
    @headers = { params: @params,
                 accept: 'application/xml' }
    @last_request = nil
  end

  def limit=(value)
    @params[:limit] = value
  end

  protected

  TIMEOUT_EXCEPTIONS = [RestClient::Exceptions::OpenTimeout,
                        RestClient::Exceptions::ReadTimeout].freeze
  def api_request(request_config)
    tries ||= @max_retries
    @last_request = request_config
    response = Request.execute request_config
    response.body
  rescue *TIMEOUT_EXCEPTIONS => err
    if (tries -= 1).positive?
      retry
    else
      message = "MAX_RETRIES exhausted: #{@max_retries}. #{err.default_message}"
      @notifier.ping message
      raise
    end
  rescue RestClient::ExceptionWithResponse => err
    message = error_message(err)
    # puts message
    @notifier.ping(message)
    # Need to find better way of doing this. Like a custom error. I'll Worry
    # about it when we turn AlmaApi in to a gem
    err.message = message
    if err.http_code == 400
      puts 'There was a 400 error for ' + @institution + ' Request: ' + @last_request.to_s
    else
      raise
    end
    # raise
  end

  def error_message(err)
    message = <<~HEREDOC
      *AlmaApi request fail*
      *HTTP Code:* #{err.http_code}
      *Institution:* #{@institution}
      *API Response:* #{err&.response.body.to_s}
      *Request:* #{@last_request}
    HEREDOC
    message
  end

end
